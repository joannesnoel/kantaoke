'use strict';

String.prototype.regexIndexOf = function(regex, startpos) {
    var indexOf = this.substring(startpos || 0).search(regex);
    return (indexOf >= 0) ? (indexOf + (startpos || 0)) : indexOf;
}

var server = 'http://creative-logik.com:8080/';

angular.module('myApp',
	[
		"ngRoute",
		"ngSanitize",
		"myApp.karaoke"
	]
)
.config(['$routeProvider', function($routeProvider) {
  $routeProvider.otherwise({redirectTo: '/karaoke'});
}]);