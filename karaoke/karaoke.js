'use strict';

angular.module('myApp.karaoke', [
	'ngRoute',
	'ngTouch',
	"com.2fdevs.videogular",
	"com.2fdevs.videogular.plugins.controls",
	"com.2fdevs.videogular.plugins.overlayplay",
	"com.2fdevs.videogular.plugins.poster"
	]
)

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/karaoke', {
    templateUrl: 'karaoke/karaoke.html',
    controller: 'KaraokeCtrl'
  });
}])
.directive('ngEnter', function () {
	    return function (scope, element, attrs) {
	        element.bind("keydown keypress", function (event) {
	            if(event.which === 13) {
	                scope.$apply(function (){
	                    scope.$eval(attrs.ngEnter);
	                });

	                event.preventDefault();
	            }
	        });
	    };
	})
	.service('Playlist', [function(){
		var Playlist = [];
		return {
			list: Playlist,
			playing: '',
			// reserve: function(song){
			// 	var data = [{
			// 		type: "video/mp4",
			// 		src: $sce.trustAsResourceUrl(server + song.id + ".mp4")
			// 	}];
			// 	Playlist.push(data);
			// 	// if(Playlist.length == 1)
			// 	// 	this.next();
			// },
			next: function(){
				if(Playlist.length > 0){
					var song = Playlist.splice(0,1)[0];
					//player.pause();
					//player.currentTime(0);
					
					// var data = {
					// 	type: "video/mp4",
					// 	src: server + song.id + ".mp4"
					// }

					// player.src(data);
					
					// player.ready(function() {
					// 	this.one('loadeddata', videojs.bind(this, function() {
					// 		this.currentTime(0);
					// 	}));

					// 	this.load();
					// 	this.play();
					// });
					player.changeSource(song);
					player.play();
				}
			}
		}
	}])
	.service('SongSearch', ['$http', function($http){
		var Songs = [];
		var compare = function compare(a,b) {
		  if (a.sort < b.sort)
		     return -1;
		  if (a.sort > b.sort)
		    return 1;
		  return 0;
		}
		return {
			all: Songs,
			find: function($scope){
				var str = $scope.searchKey;
				Songs.length = 0;
				if(str.length < 2)
					return;
				$scope.searchProcessing = true;
				var url = "https://www.googleapis.com/youtube/v3/search?key=AIzaSyBCz6mcgoseMhISuWzFXxLtTSLt3OzZq7o&q=" + str + "+karaoke&type=video&part=id,snippet&order=relevance&maxResults=50";
				//canceler.resolve();
				var items = [];
				$http.get(url)
				.success(function(data, status, headers, config){
					items = items.concat(data.items);
					$http.get(url + '&pageToken=' + data.nextPageToken)
					.success(function(data, status, headers, config){
						items = items.concat(data.items);
						$http.get(url + '&pageToken=' + data.nextPageToken)
						.success(function(data, status, headers, config){
							items = items.concat(data.items);
							$http.get(url + '&pageToken=' + data.nextPageToken)
							.success(function(data, status, headers, config){
								items = items.concat(data.items);
								$http.get(url + '&pageToken=' + data.nextPageToken)
								.success(function(data, status, headers, config){
									items = items.concat(data.items);
									$http.get(url + '&pageToken=' + data.nextPageToken)
									.success(function(data, status, headers, config){
										items = items.concat(data.items);
										$http.get(url + '&pageToken=' + data.nextPageToken)
										.success(function(data, status, headers, config){
											items = items.concat(data.items);
											$http.get(url + '&pageToken=' + data.nextPageToken)
											.success(function(data, status, headers, config){
												items = items.concat(data.items);
												$http.get(url + '&pageToken=' + data.nextPageToken)
												.success(function(data, status, headers, config){
													items = items.concat(data.items);
													$http.get(url + '&pageToken=' + data.nextPageToken)
													.success(function(data, status, headers, config){
														items = items.concat(data.items);
														angular.forEach(items, function(elem){
															var skip = false;
															var words = str.split(/\s/);
															angular.forEach(words, function(word){
																if(skip == false && elem.snippet.title.toLowerCase().indexOf(word.toLowerCase()) < 0)
																	skip = true;
															});
															if(elem.snippet.title.regexIndexOf(/[^a-z0-9]demo[^a-z0-9]/i) < 0 && skip == false){
																var item = {};
																if(elem.snippet.channelTitle == 'KaraokeOnVEVO' ||
																	elem.snippet.channelTitle == 'YouSingTheHits' ||
																	elem.snippet.channelTitle == 'singkingkaraoke' ||
																	elem.snippet.channelTitle == 'karafun' ||
																	elem.snippet.channelTitle.toLowerCase().indexOf('karaokemedia') > -1){
																	item.sort = 1;
																}else if(elem.snippet.channelTitle == 'AnywhereKY' ||
																	elem.snippet.channelTitle == 'MURAMATSUKARAOKE' ||
																	elem.snippet.channelTitle == 'jimmyzkaraoke' ||
																	elem.snippet.channelTitle == 'customizedkaraoke' ||
																	elem.snippet.channelTitle == 'singalongtv' ||
																	elem.snippet.channelTitle == 'xxnognogxx' ||
																	elem.snippet.channelId == 'UCRoAoGqqLuOIWztkcxUiYoA'){
																	item.sort = 2;
																}
																if(item.sort){
																	item.id = elem.id.videoId;
																	item.title = elem.snippet.title;
																	item.channel = elem.snippet.channelTitle;
																	Songs.push(item);
																}
															}
														});
														$scope.searchProcessing = false;
													});
												});
											});
										});
									});
								});
							});
						});
					});
					Songs.sort(compare);
				})
			}
		}
	}])
	.controller('KaraokeCtrl',
		['$sce','$scope', '$http','$timeout','SongSearch','Playlist', function ($sce,$scope,$http,$timeout,SongSearch,Playlist) {
			$scope.overlayPlayIcon = {};
			var controller = this;
			controller.onPlayerReady = function(API) {
                controller.API = API;
            };

            controller.onCompleteVideo = function() {
                $scope.next();
            };

            controller.updateState = function(state){
            	/** fix for iOS */
            // 	$timeout(function(){
            // 		if(controller.API.currentTime == 0)
        				// $scope.overlayPlayIcon = {play: true};
            // 	},500);
            };

            controller.updateTime = function(currentTime,duration){
            	/** fix for iOS */
            	// if(currentTime < 1)
            	// 	$scope.overlayPlayIcon = {};
            };


			controller.config = {
				preload: "none",
				sources: [
					{src: $sce.trustAsResourceUrl("http://creative-logik.com:8080/Iv-Xmv2yjjQ.mp4"), type: "video/mp4", title: "All of Me - John Legend"},
					// {src: $sce.trustAsResourceUrl("http://static.videogular.com/assets/videos/videogular.webm"), type: "video/webm", title: "Sample Video"},
					// {src: $sce.trustAsResourceUrl("http://static.videogular.com/assets/videos/videogular.ogg"), type: "video/ogg", title: "Sample Video"}
				],
				// tracks: [
				// 	{
				// 		src: "http://www.videogular.com/assets/subs/pale-blue-dot.vtt",
				// 		kind: "subtitles",
				// 		srclang: "en",
				// 		label: "English",
				// 		default: ""
				// 	}
				// ],
				theme: "bower_components/videogular-themes-default/videogular.css",
				// plugins: {
				// 	poster: "http://www.videogular.com/assets/images/videogular.png"
				// }
			};

			$scope.addToPlaylist = function(song){	
				$scope.status = "adding song to playlist... please wait";
				$http.post('http://creative-logik.com:8080/video', { id: song.id })
				.success(function(file, status, headers, config){
					var data = [{
						title: song.title,
						type: "video/mp4",
						src: $sce.trustAsResourceUrl(server + file)
					}];
					Playlist.list.push(data);
					if((controller.API.currentState == "stop" || controller.API.currentState == "pause") && Playlist.list.length == 1){
						$scope.next();
					}
					$scope.status = "";
				});
				$scope.searchKey = "";
				SongSearch.find($scope);
				$scope.showSearchMenu = false;
			}

			$scope.next = function(){
		
				if(Playlist.list.length > 0){
					controller.config.sources = Playlist.list.splice(0,1)[0];
					$timeout(controller.API.play.bind(controller.API), 100);
					console.log('next song...');
				}else{
					controller.API.stop();
					controller.API.clearMedia();
					console.log('no more songs...');
				}
			}

			$scope.find = function(){
				SongSearch.find($scope);
			}

			// $scope.toggleSearch = function(){
			// 	if($scope.showSearch)
			// 		$scope.showSearch = false;
			// 	else
			// 		$scope.showSearch = true;
			// }

			// $scope.togglePlaylist = function(){
			// 	if($scope.showPlaylist)
			// 		$scope.showPlaylist = false;
			// 	else
			// 		$scope.showPlaylist = true;
			// }

			$scope.swipeRight = function($event){
				$scope.showSearchMenu = true;
			}

			$scope.swipeLeft = function($event){
				$scope.showSearchMenu = false;
			}

			$scope.playlist = Playlist.list;

			$scope.results = SongSearch.all;
		}]
	);